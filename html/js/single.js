window.onload = function () {

    const domain = 'meet.jit.si';
    var element = document.getElementById('meet');

    const config = function() {
        if (meeting == 'tdsp_transmisja') {
            return {
                startWithVideoMuted: true,
                startWithAudioMuted: true,
            }
        } else {
            return {
                startWithVideoMuted: false,
                startWithAudioMuted: true,
            }
        }
    }

    const interfaceConfig = function() {
        var data;
        if (meeting == 'tdsp_transmisja') {
            data = {
                TOOLBAR_BUTTONS: [
                    'microphone', 'fullscreen', 'hangup', 'tileview',
                    // 'microphone', 'camera', 'fullscreen',
                    // 'fodeviceselection', 'hangup', 'profile', 'recording',
                    // 'livestreaming', 'etherpad', 'sharedvideo', 'settings',
                    // 'videoquality',
                ],

            }
        } else {
            data = {
                TOOLBAR_BUTTONS: [
                    'microphone', 'camera', 'hangup', 'tileview', 'raisehand'
                    // 'microphone', 'camera', 'fullscreen',
                    // 'fodeviceselection', 'hangup', 'profile', 'recording',
                    // 'livestreaming', 'etherpad', 'sharedvideo', 'settings',
                    // 'videoquality',
                ],

            }
        }
        data.DEFAULT_BACKGROUND = 'rgb(235,235,235)';
        return data;
    }

    const options = {
        roomName: meeting,
        width: '100%',
        height: '100%',
        parentNode: document.getElementById('meet'),
        configOverwrite: config(),
        interfaceConfigOverwrite: interfaceConfig(),
    };
    const api = new JitsiMeetExternalAPI(domain, options);
}