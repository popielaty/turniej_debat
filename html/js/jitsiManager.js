
class jitsiManager {

    domain = 'meet.jit.si';

    options = {
        configOverwrite: {
            startWithAudioMuted: true,
            enableTalkWhileMuted: false,
            disableTileView: false,
            enableWelcomePage: false,
            disableInitialGUM: true,
        },
    }


    defaults = {
        TOOLBAR_BUTTONS: ['tileview', 'mute-everyone'],
        SHOW_JITSI_WATERMARK: false,
        DEFAULT_BACKGROUND: 'none',
        DEFAULT_LOCAL_DISPLAY_NAME: '',
        BRAND_WATERMARK_LINK: '',
        // AUTO_PIN_LATEST_SCREEN_SHARE: 'remote-only',
        SHOW_WATERMARK_FOR_GUESTS: false,
    }

    boxes = {};
    docs = {};
    participants = {};
    screens = {};

    constructor(rooms) {
        this.rooms = rooms;
        this.setupData();
        this.setupRooms();
        this.stylizeRooms();
    }

    setupData() {
        for (let room of this.rooms) {
            this.participants[room.name] = [];
            this.screens[room.name] = [];
        }
    }

    setupRooms() {

        let options;
        this.options.interfaceConfigOverwrite = this.defaults

        for (let room of this.rooms) {
            options = this.options;
            options.roomName = room.name;
            options.width = room.width;
            options.height = room.height,
            options.parentNode = document.getElementById(room.name),
            options.interfaceConfigOverwrite.TILE_VIEW_MAX_COLUMNS = room.vertical === true ? 1 : 5;

            this.boxes[room.name] = new JitsiMeetExternalAPI(this.domain, options);
            this.boxes[room.name].isVertical = room.vertical;
        }
    }

    stylizeRooms() {
        let instance = this;
        setTimeout(function() {
            let frame;
            let box;
            for (let name in instance.boxes) {
                box = instance.boxes[name];
                frame = box.getIFrame();
                instance.docs[name] = frame.contentWindow.document;
                instance.docs[name].head.appendChild(instance.prepareStyle());
                box.executeCommand('toggleTileView');
                instance.doExtraBoxActions(name);
            }
        }, 3000);
    }

    prepareStyle() {
        let style = document.createElement('link');
        style.setAttribute('href', 'https://turniej.courtwatch.pl/css/iframe.css');
        style.setAttribute('rel', 'stylesheet');
        return style;
    }

    doExtraBoxActions(name) {
        if (this.boxes[name].isVertical === true) {
            this.doVerticalActions(name);
        } else {
            this.doFullWidthHorizontal(name);
            this.setParticipantNames(name);
            if (name === 'tdsp_juror') {
                this.setJurorVideoSize();
            }
        }
    }

    doFullWidthHorizontal(name) {
        let el = this.docs[name].getElementById('filmstripRemoteVideosContainer');
        let room = this.boxes[name]
        room.addListener('tileViewChanged', function() {
            el.removeAttribute('style');
        });
    }

    setParticipantNames(name) {
        let instance = this;
        this.boxes[name].addEventListener('participantJoined', function(user) {
            instance.addNames(name, user);
        });
        this.boxes[name].addEventListener('displayNameChange', function(user) {
            instance.changeName(name, user);
        });
    }

    setJurorVideoSize() {
        let val = 'height: 110px;min-height: 110px;min-width: 204px;width: 204px;';
        let room = this.boxes['tdsp_juror'];
        let doc = this.docs['tdsp_juror'];

        for (let event of ['tileViewChanged', 'participantJoined']) {
            room.addListener(event, function() {
                let videos = doc.getElementsByClassName('videocontainer');
                for (let video of videos) {
                    video.setAttribute('style', val);
                }
            });
        }
    }

    addNames(name, user) {
        let parent = this.docs[name].getElementById('participant_' + user.id);

        let div = document.createElement('div');
        div.classList.add('names-captions');
        div.innerText = user.displayName;
        parent.appendChild(div);
    }

    changeName(name, user) {
        let parent = this.docs[name].getElementById('participant_' + user.id);
        let div = parent.getElementsByClassName('names-captions')[0]
        div.innerText = user.displayname;
    }

    doVerticalActions(name) {
        this.docs[name].body.classList.add('vertical');
        this.doSortableContainers(name);

    }

    doSortableContainers(name) {
        let instance = this;
        this.boxes[name].addEventListener('participantJoined', function(user) {
            instance.setParticipants(name);
            instance.addArrows(name, user.id);
            instance.rearrangeScreens(name);

        });

        this.boxes[name].addEventListener('participantLeft', function(user) {
            instance.setParticipants(name);
            instance.rearrangeScreens(name);
        });
    }

    setParticipants(name) {
        let participants = [];
        for (let user of this.boxes[name].getParticipantsInfo()) {
            if (user.displayName !== 'transmisja') {
                participants.push(user.participantId);
            }
        }
        this.participants[name] = participants;
    }

    addArrows(name, user_id) {
        let element = this.docs[name].getElementById('participant_' + user_id);
        this.screens[name][user_id] = element;
        element.appendChild(this.getArrows(name, user_id));
    }

    getArrows(name, user_id) {
        let div = document.createElement('div');
        div.classList.add('arrows');
        let up = document.createElement('a');
        up.innerHTML = '&uarr;';
        let down = document.createElement('a');
        down.innerHTML = '&darr;';
        div.appendChild(up);
        div.appendChild(down);

        let instance = this;

        up.addEventListener('click', function(e) {
            instance.moveParticipant(e, name, user_id, false);
        })

        down.addEventListener('click', function(e) {
            instance.moveParticipant(e, name, user_id, true);
        });
        return div;
    }

    moveParticipant(e, name, user_id, is_down) {
        e.preventDefault();
        e.stopImmediatePropagation();
        let participants = this.participants[name];
        let key = participants.indexOf(user_id);

        if (this.canMove(key, participants.length, is_down) === false) return;

        let other_key = is_down === true ? key + 1 : key - 1;
        [participants[key], participants[other_key]] = [participants[other_key], participants[key]];
        this.participants[name] = participants;
        this.rearrangeScreens(name);
    }

    canMove(key, length, is_down) {
        if (key === -1) return false;
        if (key === 0 && is_down === false) return false;
        if (key+1 === length && is_down === true) return false;
        return true;
    }

    rearrangeScreens(name) {
        let strip = this.docs[name].getElementById('filmstripRemoteVideosContainer');

        for (let id of this.participants[name]) {
            strip.appendChild(this.screens[name][id]);
        }


    }

}

