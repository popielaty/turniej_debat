class dropdownMaker {

    data;
    container;
    is_id;
    type;
    dropdown = null;
    dropdowns = [];
    listening_el = null;

    constructor(type, listening_el_id = null) {
        this.type = type;
        this.listening_el = this.setListeningEl(listening_el_id);
        this.prepareDropdowns();
    }

    setListeningEl(el_id) {
        if (el_id === null) return null;
        return document.getElementById(el_id);
    }

    async prepareDropdowns() {
        this.data = await this.getData();

        this.setContainer();

        if (this.is_id) {
            this.setDropdownsById();
        } else {
            this.setDropdownByCollection();
        }
        this.setListener();
    }

    async getData() {
        let data = await fetch('/js/vars.json')
            .then(results => results.json())
            .then(json => {
                let date = new Date().toISOString();
                date = date.substr(0, 10);
                return typeof json[date] === 'undefined' ? json.placeholder : json[date];
            });
        return data[this.type];
    }

    setContainer() {
        let el = document.getElementById(this.type);
        if (el === null) {
            this.container = document.getElementsByClassName(this.type);
            this.is_id = false;
        } else {
            this.container = el;
            this.is_id = true;
        }
    }

    setDropdownsById() {
        this.is_preentered = this.container.hasAttribute('preentered');
        this.createChildren();
    }

    setDropdownByCollection() {
        for (let element of this.container) {
            element.appendChild(this.getDropdown());
        }
    }

    createChildren() {
        let i = 1;
        let li;
        let dropdown;
        while (i <= this.data.length) {
            dropdown = this.getDropdown();
            if (this.is_preentered) dropdown.selectedIndex = i;
            li = document.createElement('li');
            li.appendChild(dropdown);
            this.container.appendChild(li);
            i++;
        }
    }

    getDropdown() {
        if (this.dropdown === null) {
            this.dropdown = this.setDropdown();
        }
        let dropdown =  this.dropdown.cloneNode(true);
        this.dropdowns.push(dropdown);
        return dropdown;
    }

    setDropdown() {
        let items = this.data;
        let option;

        let select = document.createElement('select');
        select.classList.add('dropdown');
        select.appendChild(document.createElement('option'));
        for (let item of items) {
            option = document.createElement('option');
            option.setAttribute('value', item);
            option.innerText = item;
            select.appendChild(option);
        }
        return select;
    }

    setListener() {
        if (this.listening_el === null) return;

        let listening_el = this.listening_el;

        for (let dropdown of this.dropdowns) {
            dropdown.addEventListener('change', function(e) {
                listening_el.value = e.target.value;
                dropdown.selectedIndex = 0;
            });
        }

        
    }

}