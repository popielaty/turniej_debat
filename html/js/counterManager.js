class counterManager {

    time;
    protected = 30;
    timer;
    speech;
    advocem;
    pause;
    interval;
    is_playing = false;

    constructor() {
        this.timer = document.getElementById('timer');
        this.speech = document.getElementById('speech');
        this.advocem = document.getElementById('advocem');
        this.pause = document.getElementById('pause');

        this.setEvents();
    }

    setEvents() {
        let instance = this;

        this.speech.addEventListener('click', function(){
            instance.doSpeech();
        });

        this.advocem.addEventListener('click', function(){
            instance.doAdvocem();
        });

        this.pause.addEventListener('click', function(){
            instance.doPause();
        });

    }

    doSpeech() {
        clearInterval(this.interval);
        this.time = 60*4;
        this.countTime();
    }

    doAdvocem() {
        clearInterval(this.interval);
        this.time = 30;
        this.countTime();
    }

    doPause() {
        if (this.is_playing === true) {
            clearInterval(this.interval);
            this.is_playing = false;
            this.pause.innerText = 'PLAY';
        } else if (this.is_playing === false) {
            this.countTime();
        }
    }

    countTime() {
        if (this.time > this.protected) {
            this.timer.classList.remove('protected');
        }
        this.is_playing = true;
        this.pause.innerText = 'PAUSE';
        let mins;
        let secs;
        let instance = this;
        this.interval = setInterval(function() {
            mins = Math.floor(instance.time / 60)
            secs = instance.time % 60;
            instance.timer.innerText = mins + ':' + (secs > 9 ?  secs : '0' + secs);
            if (instance.time == instance.protected) {
                instance.timer.classList.add('protected');
            }
            instance.time --;
            if (instance.time < 0) {
                instance.is_playing = pause;
                clearInterval(instance.interval);
                instance.finishCounting();
            }
        }, 1000);
    }

    finishCounting() {
        let count = 6;
        let i = 0;
        let instance = this;
        let interval = setInterval(function() {
            instance.timer.classList.toggle('fade');
            i++;
            if (i === count) {
                clearInterval(interval);
                instance.timer.classList.remove('protected');
            }
        }, 500);
    }

}